#include "sqlite3.h"
#include <iostream>
#include <string>

using namespace std;

int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	int i;
	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("PAUSE");
		return 1;
	}

	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string);", NULL, 0, &zErrMsg);

	rc = sqlite3_exec(db, "insert into people(name) values('Shachar');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values('Barak');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values('Keren');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "select * from people", callback, 0, &zErrMsg);
	rc = sqlite3_exec(db, "update people set name = 'Paz' where id = last_insert_rowid();", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "select * from people;", callback, 0, &zErrMsg);

	sqlite3_close(db);
	system("PAUSE");
	return 0;
}