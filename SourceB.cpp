#include "sqlite3.h"
#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>

using namespace std;

unordered_map<string, vector<string>> results;

int callback(void *NotUsed, int argc, char **argv, char **azColName);
void clearTable();
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
void messageForUser2(bool result, int from, int to, int priceFrom, int priceTo, int amount);
void messageForUser1(bool result, int buyer, int car, int price, int balance, int available);

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("PAUSE");
		return 1;
	}

	bool check1 = carPurchase(1, 1, db, zErrMsg);
	bool check2 = carPurchase(11, 20, db, zErrMsg);
	bool check3 = carPurchase(6, 4, db, zErrMsg);

	bool check4 = balanceTransfer(3, 5, 150, db, zErrMsg);

	sqlite3_close(db);
	system("PAUSE");
	return 0;
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}
	return 0;
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	bool result = true;
	string command = "", temp = "";
	int balance, price, available;
	int rc;

	//get the buyer's balance
	command = "select balance from accounts where buyer_id=" + to_string(buyerid);
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	auto it = results.begin();
	temp = it->second.at(0);
	balance = stoi(temp);
	clearTable();

	//get the car's availability
	command = "select available from cars where id=" + to_string(carid);
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	it = results.begin();
	temp = it->second.at(0);
	available = stoi(temp);
	clearTable();

	//get the car's price
	command = "select price from cars where id=" + to_string(carid);
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	it = results.begin();
	temp = it->second.at(0);
	price = stoi(temp);
	clearTable();
	
	//if the car isn't available
	//or
	//if the buyer doesn't have enough money to buy it
	if ((available == 0) || (available == 1 && balance < price))
	{
		result = false;
	}
	else	//if he can buy it
	{
		command = "update cars set available=0 where id=" + to_string(carid);	//the car isn't available anymore
		rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
		command = "update accounts set balance=" + to_string(balance - price) + " where buyer_id=" + to_string(buyerid);	//update the buyer's balance
		rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	}
	
	//message for the user
	messageForUser1(result, buyerid, carid, price, balance, available);
	clearTable();
	return result;
}

//message for the user
void messageForUser1(bool result, int buyer, int car, int price, int balance, int available)
{
	string isAvailable = (available == 1) ? " is available." : " isn't available.";
	string isSuccessful = (result == 1) ? "The purchase was successful!" : "The purchase whan't successful..";
	cout << "buyer " << buyer << " have " << balance << endl;
	cout << "car " << car << " costs " << price << " and it" << isAvailable << endl;
	cout << isSuccessful << endl << endl;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	bool result = false;
	string command = "", temp = "";
	int priceFrom, priceTo;

	//get the first buyer's balance
	command = "select balance from accounts where buyer_id=" + to_string(from);
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	auto it = results.begin();
	temp = it->second.at(0);
	priceFrom = stoi(temp);
	clearTable();

	//get the second buyer's balance
	command = "select balance from accounts where buyer_id=" + to_string(to);
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	it = results.begin();
	temp = it->second.at(0);
	priceTo = stoi(temp);
	clearTable();

	if (amount <= priceFrom)	//check if the first buyer have enough money so he won't stay with minus
	{
		rc = sqlite3_exec(db, "begin transaction", callback, 0, &zErrMsg);
		command = "update accounts set balance=" + to_string(amount + priceTo) + " where id=" + to_string(to);	//add the amount to the second buyer
		rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
		command = "update accounts set balance=" + to_string(priceFrom - amount) + " where id=" + to_string(from);	//subtract the amount from the first buyer
		rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
		rc = sqlite3_exec(db, "commit", callback, 0, &zErrMsg);
		result = true;
	}

	messageForUser2(result, from, to, priceFrom, priceTo, amount);
	return result;
}

//message for the user
void messageForUser2(bool result, int from, int to, int priceFrom, int priceTo, int amount)
{
	if (result)
	{
		cout << "The transformation was successful!" << endl;
		cout << "buyer " << from << " had " << priceFrom << ". He gave " << amount << " to buyer " << to << endl;
		cout << "Now buyer " << from << " have " << (priceFrom - amount) << " and buyer " << to << " have " << (priceTo + amount) << endl;
	}
	else
	{
		cout << "The transformation wasn't successful.." << endl;
		cout << "buyer " << from << " had only " << priceFrom << ". He can't give " << amount << " to buyer " << to << endl;
	}
}